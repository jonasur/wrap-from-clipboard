import pyperclip

pattern = "A\n{0}\nZ"

pyperclip.copy(pattern.format(pyperclip.paste()))

print("Copied to clipboard")